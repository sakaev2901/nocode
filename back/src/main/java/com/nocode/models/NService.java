package com.nocode.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NService {
    private Long id;
    private String name;
    private Integer price;
    private List<NClass> classes;
    private List<NFile> files;
}
