package com.nocode.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NProject {
    private Long id;
    private String name;
    private Boolean isActive;
    private List<NService> services;
}
