package com.nocode.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NClass {
    private Long id;
    private Package pckg;
//    private String access;
    private String name;
    private String content;
//    private List<Annotation> annotations;
//    private List<Class> implementsClasses;
//    private List<Class> extendsClasses;
//    private List<Import> imports;
//    private List<Field> fields;
//    private List<Method> methods;
//    private List<Constructor> constructors;

}
