package com.nocode.repositories;

import com.nocode.models.NProject;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectRepositoryImpl {

    private static final String INSERT_QUERY = "insert into project (name, is_active) values (:name, :is_active)";
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public ProjectRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public long create(NProject project) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("name", project.getName());
        parameterSource.addValue("is_active", project.getIsActive());
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(INSERT_QUERY, parameterSource, key, new String[]{"id"});
        return key.getKey().longValue();
    }
}
