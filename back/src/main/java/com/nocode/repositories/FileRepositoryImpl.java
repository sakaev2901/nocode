package com.nocode.repositories;

import com.nocode.models.NFile;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class FileRepositoryImpl {

    private static final String INSERT_QUERY = "insert into file (path, content, service_id) VALUES (:path, :content, :serviceId)";
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public FileRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public long create(NFile file, Long serviceId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("path", file.getPath());
        parameterSource.addValue("content", file.getContent());
        parameterSource.addValue("serviceId", serviceId);
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(INSERT_QUERY, parameterSource, key, new String[]{"id"});
        return key.getKey().longValue();
    }
}
