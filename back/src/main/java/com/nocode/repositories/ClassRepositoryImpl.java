package com.nocode.repositories;

import com.nocode.models.NClass;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class ClassRepositoryImpl {

    private static final String INSERT_QUERY = "insert into class (name, package, content, service_id) values (:name, :package, :content, :serviceId)";
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public ClassRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public long create(NClass nClass, Long serviceId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("name", nClass.getName());
        parameterSource.addValue("content", nClass.getContent());
        parameterSource.addValue("package", nClass.getPckg().getName());
        parameterSource.addValue("serviceId", serviceId);
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(INSERT_QUERY, parameterSource, key, new String[]{"id"});
        return key.getKey().longValue();
    }
}
