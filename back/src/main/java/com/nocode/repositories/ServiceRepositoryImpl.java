package com.nocode.repositories;

import com.nocode.models.NService;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceRepositoryImpl {

    private static final String INSERT_QUERY = "insert into service (name, price) values (:name, :price)";
    private static final String LINK_WITH_PROJECT_QUERY = "insert into project_service values (:projectId, :serviceId)";
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public ServiceRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public long create(NService service) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("name", service.getName());
        parameterSource.addValue("price", service.getPrice());
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(INSERT_QUERY, parameterSource, key, new String[]{"id"});
        return key.getKey().longValue();
    }

    public int linkWithProject(Long serviceId, Long projectId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("serviceId", serviceId);
        parameterSource.addValue("projectId", projectId);
        return jdbcTemplate.update(LINK_WITH_PROJECT_QUERY, parameterSource);
    }
}
