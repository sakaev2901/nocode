package com.nocode;

import com.nocode.models.NService;
import com.nocode.services.NServiceServiceImpl;
import com.nocode.services.ProjectServiceImpl;
import com.nocode.services.ScannerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class NocodeApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(NocodeApplication.class, args);
//        ProjectService service = context.getBean(ProjectService.class);
//        service.createBaseProjectStructure(UUID.randomUUID().toString());
//        var controllerCreator = context.getBean(ControllerCreator.class);
//        var service = context.getBean(NServiceServiceImpl.class);
//        ScannerService scannerService = new ScannerService();
//        NService baseService = scannerService.scan();
//        service.save(baseService);
//        controllerCreator.createController();
    }

}
