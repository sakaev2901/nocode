package com.nocode.interfaces;

public interface ProjectService {

    boolean createBaseProjectStructure(String projectName);
}
