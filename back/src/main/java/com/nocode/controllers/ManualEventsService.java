package com.nocode.controllers;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.AbstractResource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class ManualEventsService  {
    private static final String TEMPLATE_FILE_PATH = "classpath:ManualEventTemplate.xlsx";

    private final ResourceLoader resourceLoader;

    public ManualEventsService(
                               ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }


    public DTO getTemplateFile(Integer scenarioId) throws IOException {

//        byte[] content = IOUtils.toByteArray(new FileInputStream(resourceLoader.getResource(TEMPLATE_FILE_PATH)));
        System.out.println(resourceLoader.getResource(TEMPLATE_FILE_PATH).getFile().getName());
        byte[] content = Files.readAllBytes(resourceLoader.getResource(TEMPLATE_FILE_PATH).getFile().toPath());
        return DTO.builder().fileName(resourceLoader.getResource(TEMPLATE_FILE_PATH).getFile().getName())
                .resource( new ByteArrayResource(content))
                .build();
    }

}
