package com.nocode.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.AbstractResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
public class Controller {

    @Autowired
    private ManualEventsService manualEventsService;

    @PutMapping("/test")
    public void test() {
        System.out.println("Hello");
    }

    @GetMapping("/file")
    public ResponseEntity<?> getFile() {
//        try {
        try {
            DTO dto = manualEventsService.getTemplateFile(1);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", dto.getFileName()));
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(dto.getResource().contentLength())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(dto.getResource());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

//        } catch (Exception e) {
//            return ResponseEntity.badRequest().body("File fetching error");
//        }

    }
}
