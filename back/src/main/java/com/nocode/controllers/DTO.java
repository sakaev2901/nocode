package com.nocode.controllers;

import lombok.Builder;
import lombok.Data;
import org.springframework.core.io.AbstractResource;
@Builder
@Data
public class DTO {
    private AbstractResource resource;
    private String fileName;
}
