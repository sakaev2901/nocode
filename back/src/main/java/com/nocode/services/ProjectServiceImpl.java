package com.nocode.services;

import com.nocode.interfaces.ProjectService;
import com.nocode.models.NProject;
import com.nocode.models.NService;
import com.nocode.repositories.ProjectRepositoryImpl;
import com.nocode.repositories.ServiceRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {
//    private static String PROJECTS_PATH = "C:\\Projects\\nocode\\userprojects\\test1\\";
    private static String PROJECTS_PATH = "C:\\Projects\\nocode\\userprojects\\";
//    private static String BASE_PROJECT_PATH = "C:\\Projects\\nocode\\userprojects\\test2\\";
    private static String BASE_PROJECT_PATH = "classpath:baseProject";

    @Autowired
    ResourceLoader resourceLoader;
    private final ScannerService scannerService;
    private final ProjectRepositoryImpl projectRepository;
    private final ServiceRepositoryImpl serviceRepository;

    public ProjectServiceImpl(
            ScannerService scannerService,
            ProjectRepositoryImpl projectRepository,
            ServiceRepositoryImpl serviceRepository) {
        this.scannerService = scannerService;
        this.projectRepository = projectRepository;
        this.serviceRepository = serviceRepository;
    }

    public void save() {
        NService baseService = scannerService.scan();
        NProject project = NProject.builder()
                .isActive(false)
                .name("test")
                .services(List.of(baseService))
                .build();
        Long projectId = projectRepository.create(project);
        Long serviceId = serviceRepository.create(project.getServices().get(0));
        serviceRepository.linkWithProject(serviceId, projectId);
    }

    @Override
    public boolean createBaseProjectStructure(String projectName) {
        var baseProjectResource = resourceLoader.getResource(BASE_PROJECT_PATH);
        String baseProjectPath = null;
        try {
            baseProjectPath = baseProjectResource.getFile().getAbsolutePath();
            copyDirectory(baseProjectPath, PROJECTS_PATH + projectName);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        }
        return true;
    }

    public static void copyDirectory(String sourceDirectoryLocation, String destinationDirectoryLocation)
            throws IOException {
        Files.walk(Paths.get(sourceDirectoryLocation))
                .forEach(source -> {
                    Path destination = Paths.get(destinationDirectoryLocation, source.toString()
                            .substring(sourceDirectoryLocation.length()));
                    try {
                        Files.copy(source, destination);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

}
