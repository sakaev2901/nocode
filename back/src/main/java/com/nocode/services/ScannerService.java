package com.nocode.services;

import com.nocode.models.*;
import com.nocode.models.NClass;
import com.nocode.models.Package;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class ScannerService {
    private final String projectPath = "C:\\Projects\\base_service\\";
    private final String javaSourcePath = "src\\main\\java\\";
    private final String resourcePath = "src\\main\\resources\\";
    private final String basePackage = "nocode.base";

    @SneakyThrows
    public NService scan() {
        NService service = NService.builder()
                .name("base_service")
                .price(0)
                .build();
        String baseDir = basePackage.replace(".", "\\");
        System.out.println(baseDir);
        File javaSourceDir = new File(projectPath + javaSourcePath + baseDir);
        File resourceDir = new File(projectPath + resourcePath);

        System.out.println(javaSourceDir.isDirectory());
        List<File> files = Arrays.asList(Objects.requireNonNull(javaSourceDir.listFiles()));
        List<File> javaFiles = files.stream()
                .filter(File::isFile)
                .collect(Collectors.toList());
        File javaFile = javaFiles.get(0);
        String content = IOUtils.toString(new FileInputStream(javaFile));
        System.out.println(content);
        List<NClass> classes = Files.walk(Paths.get(javaSourceDir.getAbsolutePath()))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .map(file -> {
                    try {
                        return new FileInputStream(file);
                    } catch (FileNotFoundException e) {
                        throw new IllegalStateException(e);
                    }
                })
                .map(this::scanClass)
                .collect(Collectors.toList());
        List<NFile> resourceFiles = Files.walk(Paths.get(resourceDir.getAbsolutePath()))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .map(file -> NFile.builder()
                        .path(file.getAbsolutePath().replace(projectPath, ""))
                        .content(readFile(file))
                        .build())
                .collect(Collectors.toList());
        File pomFile = new File(projectPath + "pom.xml");
        resourceFiles.add(NFile.builder()
                .path(pomFile.getAbsolutePath().replace(projectPath, ""))
                .content(readFile(pomFile))
                .build());
        resourceFiles.forEach(System.out::println);
        service.setClasses(classes);
        service.setFiles(resourceFiles);
        return service;
    }

    @SneakyThrows
    public String readFile(File file) {
        return IOUtils.toString(new FileInputStream(file));
    }

    @SneakyThrows
    public NClass scanClass(InputStream inputStream) {
        String classContent = IOUtils.toString(inputStream);
        Pattern pattern = Pattern.compile(".* (class|interface) (.*)( implements | extends |( implements.*| extends.*){0} \\{)");
        Matcher matcher = pattern.matcher(classContent);
        NClass c = new NClass();
        if (matcher.find()) {
            c.setName(matcher.group(2).split(" ")[0]);
            c.setContent(classContent);
        }
        Pattern pattern1 = Pattern.compile("package (.*);");
        matcher = pattern1.matcher(classContent);
        if (matcher.find()) {
            c.setPckg(new Package(matcher.group(1)));
        }
        return c;
    }

//    @SneakyThrows
//    public File scanFile(Input)

    public static void main(String[] args) {
        ScannerService service = new ScannerService();
        System.out.println(service.scan());
    }
}
