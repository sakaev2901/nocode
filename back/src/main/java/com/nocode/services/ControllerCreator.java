package com.nocode.services;

import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class ControllerCreator {

    private String projectPath = "C:\\Projects\\nocode\\userprojects\\7a0a154e-4559-4336-948f-6844f4e80f23\\" +
            "src\\main\\java\\com\\nocode\\baseproject\\";
    private String basePackage = "com.nocode.baseProject";
    private String baseClassDir = "classpath:classTemplates\\Class.java";

    public void createController() {
        createClass("TestController.java", "controllers");
    }

    private void createClass(String className, String packageName) {
        var packageDir = packageName.replace(".", "\\") + "\\";
        var classDir = projectPath + packageDir;
        new File(classDir).mkdir();
        var classFile = new File(projectPath + packageDir + className);
        try {
            classFile.createNewFile();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try(var writer = new BufferedWriter(new FileWriter(classFile));) {

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private String getTemplatedClass(String className, String packageName) {
//        var packageDir = packageName.replace(".", "\\") + "\\";
//        var classPath = projectPath + packageDir + className;
//        try(var reader = new BufferedReader(new FileReader(classPath))) {
//
//        }
        return null;
    }

}
