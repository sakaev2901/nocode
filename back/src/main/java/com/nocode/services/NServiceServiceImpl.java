package com.nocode.services;

import com.nocode.models.NService;
import com.nocode.repositories.ClassRepositoryImpl;
import com.nocode.repositories.FileRepositoryImpl;
import com.nocode.repositories.ServiceRepositoryImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NServiceServiceImpl {

    private final ServiceRepositoryImpl serviceRepository;
    private final ClassRepositoryImpl classRepository;
    private final FileRepositoryImpl fileRepository;

    public NServiceServiceImpl(ServiceRepositoryImpl serviceRepository, ClassRepositoryImpl classRepository, FileRepositoryImpl fileRepository) {
        this.serviceRepository = serviceRepository;
        this.classRepository = classRepository;
        this.fileRepository = fileRepository;
    }

    @Transactional
    public void save(NService service) {
        Long serviceId = serviceRepository.create(service);
        service.getClasses().forEach(nClass -> classRepository.create(nClass, serviceId));
        service.getFiles().forEach(nFile -> fileRepository.create(nFile, serviceId));
    }
}
